import Dependencies._
import sbt._
//import com.gilcloud.sbt.gitlab.{GitlabCredentials,GitlabPlugin}

val vs_base = "0.1.1"
val vs_tag = sys.env.get("CI_COMMIT_BRANCH") match {
  case Some("master") => ""
  case _ => s"-SNAPSHOT"
}

ThisBuild / scalaVersion     := "2.13.2"
ThisBuild / version          := "0.1.0"
ThisBuild / organization     := "net.benlist"
ThisBuild / organizationName := "benlist"

lazy val root = (project in file("."))
  .settings(
    name                := "scala-docker-test",
    dockerBaseImage     := "openjdk:8-jre-alpine",
    credentials         += Credentials(Path.userHome / ".sbt" / ".credentials.gitlab"),
    resolvers           += "gitlab" at "https://gitlab.com/api/v4/projects/20820514/packages/maven",
    libraryDependencies += "net.benlist" %% "scala-publish-test" % "0.2.0",
    libraryDependencies += scalaTest % Test
  ).enablePlugins(JavaAppPackaging,DockerPlugin,AshScriptPlugin)

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
